<?php

declare(strict_types=1);

namespace TheoD02\EasyCodingStandard;

use Symplify\EasyCodingStandard\Console\Output\ExitCodeResolver;
use Symplify\EasyCodingStandard\Console\Style\EasyCodingStandardStyle;
use Symplify\EasyCodingStandard\Contract\Console\Output\OutputFormatterInterface;
use Symplify\EasyCodingStandard\ValueObject\Configuration;
use Symplify\EasyCodingStandard\ValueObject\Error\ErrorAndDiffResult;
use function htmlspecialchars;
use function implode;
use function json_encode;
use function sprintf;
use const ENT_COMPAT;
use const ENT_XML1;
use const JSON_PRETTY_PRINT;
use const JSON_THROW_ON_ERROR;

final class GitlabOutputFormatter implements OutputFormatterInterface
{
    public const NAME = 'gitlab';

    private readonly EasyCodingStandardStyle $easyCodingStandardStyle;

    private readonly ExitCodeResolver $exitCodeResolver;

    public function __construct(EasyCodingStandardStyle $easyCodingStandardStyle, ExitCodeResolver $exitCodeResolver)
    {
        $this->easyCodingStandardStyle = $easyCodingStandardStyle;
        $this->exitCodeResolver = $exitCodeResolver;
    }

    public function report(ErrorAndDiffResult $errorAndDiffResult, Configuration $configuration): int
    {
        $json = $this->createJsonOutput($errorAndDiffResult);
        $this->easyCodingStandardStyle->writeln($json);

        return $this->exitCodeResolver->resolve($errorAndDiffResult, $configuration);
    }

    public function getName(): string
    {
        return self::NAME;
    }

    private function createJsonOutput(ErrorAndDiffResult $errorAndDiffResult): string
    {
        $errors = [];

        foreach ($errorAndDiffResult->getErrors() as $codingStandardError) {
            $fileName = $codingStandardError->getRelativeFilePath();
            $errors[] = [
                'description' => sprintf(
                    '%s:%s',
                    $codingStandardError->getFileWithLine(),
                    $codingStandardError->getMessage()
                ),
                'content' => [
                    'body' => htmlspecialchars($codingStandardError->getDiff(), ENT_COMPAT | ENT_XML1),
                ],
                'category' => ['lint'],
                'severity' => 'blocker',
                'location' => [
                    'path' => $fileName,
                    'lines' => [
                        'begin' => $codingStandardError->getLine(),
                    ],
                ],
            ];
        }

        foreach ($errorAndDiffResult->getFileDiffs() as $codingStandardError) {
            $fileName = $codingStandardError->getRelativeFilePath();
            $errors[] = [
                'description' => 'Please fix ECS errors for fixing ' . implode(
                    ', ',
                    $codingStandardError->getAppliedCheckers()
                ),
                'severity' => 'blocker',
                'content' => [
                    'body' => htmlspecialchars($codingStandardError->getDiff(), ENT_COMPAT | ENT_XML1),
                ],
                'location' => [
                    'path' => $fileName,
                    'lines' => [
                        'begin' => 0,
                    ],
                ],
            ];
        }

        return json_encode($errors, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT);
    }
}
